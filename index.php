<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title></title>
		<?php 
			require('connection.php');

			$sql = "SELECT * FROM tasks";
			$completed_qry = "SELECT * FROM tasks where completed=1";

			if (isset($_GET['q']) && !empty($_GET['q']))
			{
				$sql = $sql . ' order by ' . $_GET['q'] . ' asc';
			}

			$result = $conn->query($sql);
			$completed = $conn->query($completed_qry);

		?>
		<link rel="stylesheet" type="text/css" href="css/styles.css">
		<script src="https://code.jquery.com/jquery-3.4.1.js"></script>
      	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"/>
</head>
<body>

<body>
      <div class="center">
         <div class="button">
            <span class="text"><label id="count"><?php echo $result->num_rows; ?></label> Tasks</span>
            <span class="text"><label id="completed-lbl"><?php echo $completed->num_rows; ?></label> Completed</span>
            <span class="icon">
            	<select id="sort">
            		<option value="">Sort</option>
            		<option value="priority" <?php echo isset($_GET['q']) && ($_GET['q']) == 'priority' ? 'selected':'' ?> >Priority</option>
            		<option value="name" <?php echo isset($_GET['q']) && ($_GET['q']) == 'name' ? 'selected':'' ?> >Name</option>
            	</select>
            </span>
         </div>
         <div class="field">
            <input type="text" required placeholder="Add a task">
            <span class="add-btn">ADD</span>
         </div>
         <ul>
			<?php 
				if ($result->num_rows >	 0) {
				  // output data of each row
				  while($row = $result->fetch_assoc()) {
					echo '<tr>';
						echo '<li><span title="Delete task" class="delete-task" id="'. $row['id'] .'"><i class="fa fa-trash"></i></span><span title="Complete task"><input type="checkbox" ' . ($row['completed'] == 1 ? 'checked':'') .' data-value="1" class="completed" data-id="'. $row['id'] .'"></span>'. ucfirst($row['name']).
								'<select class="priority" data-id="'. $row['id'] .'">
									<option value="1" '. ($row['priority'] == 1 ? 'selected':'') .'>High</option>
									<option value="2" '. ($row['priority'] == 2 ? 'selected':'') .' >Low</option>
								</select></li>';
					echo '</tr>';
				  }
				}

			 ?>
         </ul>
      </div>

   </body>
   <script type="text/javascript" src="js/script.js"></script>
</html>
<?php 
	$conn->close();
 ?>