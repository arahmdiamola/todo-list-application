
         $('.add-btn').click(function(){

         	$.ajax({
         		url:'save-task.php',
         		method:'post',
         		data: {'task':$('input').val()},
         		success: function(e){
		           	$('ul').append("<li><span class='delete-task' id='"+ e +"'><i class='fa fa-trash'></i></span><span title='Complete task'><input type='checkbox' class='completed' data-value='1' data-id='"+ e +"'></span>"+ $('input').val() +"<select class='priority' data-id='"+ e +"'>\
		           		<option value='1'>High</option>\
		           		<option value='2' selected>Low</option></select></li>");
		            $('input').val("");
		            $('#count').text($('li').length);

         		}
         	});


         });
         $(document).on("click", '.delete-task', function(y){
         	var that = $(this);
         	$.ajax({
	     		url:'delete-task.php',
	     		method:'post',
	     		data: {'id': this.id },
	     		success: function(e){
		           	$(that).parent().remove();
					$('#count').text($('li').length);
					$('#completed-lbl').text($( ".completed:checked" ).length);
	     		}
	     	});
         });

         $('#sort').on('change', function(){
         	if($(this).val() != '')
         	{
         		location.href = '?q='+ $(this).val();
         	}
         	else
         	{
         		location.href = '/todolist';;
         	}
         });

         $(document).on('change', '.priority, .completed', function(){
         	var field = $(this).attr('class');
         	
         	var value = 'null';

         	if( field == 'completed' && $(this).is(':checked'))
         	{
         		value = 1;
         	}
         	else if( field == 'priority')
         	{
         		value = $(this).val();
         	}

         	$.ajax({
	     		url:'update-task.php',
	     		method:'post',
	     		data: {
	     				'id': $(this).attr('data-id'),
	     				'field': field,
	     				'value': value
	     			  },
	     		success: function(e){
		           	$('#completed-lbl').text($( ".completed:checked" ).length);
		           	location.reload();
	     		}
	     	});
         });